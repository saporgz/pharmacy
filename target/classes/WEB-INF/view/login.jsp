<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Archi Medicos</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <style>
       .required-field{
              color:red;
              font-weight: bold;
       }
       .modal-header-primary {
       		color:#fff;
		    padding:9px 15px;
		    border-bottom:1px solid #eee;
		    background-color: #428bca;
		    -webkit-border-top-left-radius: 5px;
		    -webkit-border-top-right-radius: 5px;
		    -moz-border-radius-topleft: 5px;
		    -moz-border-radius-topright: 5px;
		     border-top-left-radius: 5px;
		     border-top-right-radius: 5px;
		}
  </style>  
</head>
<body>
<div class="container">   
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                   
            <div class="panel panel-info" >
                    <div class="panel-heading">				
                        <div class="panel-title">Sign In</div>
                        <!--<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>-->
                    </div>   
 					
 
                    <div style="padding-top:30px" class="panel-body" >                                                
                        <form id="loginform" class="form-horizontal" role="form" method="post" action="loginAuthentication.html">

                            <div style="margin-bottom: 25px" class="input-group">
                                 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                 <input id="login-username" type="text" class="form-control" name="userName" placeholder="Username" required="required"  autofocus="autofocus" value="admin"/>                                       
                            </div>
                               
                            <div style="margin-bottom: 25px" class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                  <input id="login-password" type="password" class="form-control" name="password" placeholder="Password" required="required" value="admin" />
                            </div>
                          
                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->
                                <div class="col-sm-12 controls">
                                  <input type="submit" class="btn btn-success" value="Login">
                                  <input type="reset" class="btn  btn-info" value="Clear">
                                </div>
                            </div>
                                               
                                  <div style="display:none;" id="login-alert" class="alert alert-danger col-sm-12"></div>                            
                                 
                            <div class="form-group">
                                <div class="col-md-12 control">
                                    <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                        Don't have an account!
                                    <a href="#" data-toggle="modal" data-target="#userRegistration" data-backdrop="static" data-keyboard="false" data-controls-modal="#userRegistration">
                                        Sign Up Here
                                    </a>
                                    </div>
                                </div>
                            </div>   
                        </form>
                        <c:if test="${login ne null}">			
							<c:if test="${login.message ne null}">
								<div class="alert alert-danger">
									<strong>Authentication Failed.!!! Try Again...</strong>	
								</div>
							</c:if>						
						</c:if>
						<c:if test="${logoutMessage ne null}">	
							<div class="alert alert-success">
								<strong>${logoutMessage}</strong>	
							</div>					
						</c:if>
                    </div>                                  
            </div> 
        </div>          
    </div>    
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>              
</body>
</html> 