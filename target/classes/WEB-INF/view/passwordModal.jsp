<!-- Navigation -->
        	<div class="overlay"></div>    
		        <!-- Sidebar -->
		        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
		            <ul class="nav sidebar-nav">
		                <li class="sidebar-brand">
		                    <a href="#">
		                       <img src="image/finetab.png" alt="FineTab" class="img-responsive center-block" />  
		                    </a>
		                </li>
						<li>
							<a href="#" data-toggle="modal" data-target="#userDetails" data-backdrop="static" data-keyboard="false" data-controls-modal="#userDetails">
                        	<i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
						
						<li>
                            <a href="dashboard.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>				
						
						                                                
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-wrench fa-fw"></i>
								Administration <span class="caret"></span>
							</a>
                            <ul class="dropdown-menu" role="menu">
                            	<li></li>
                                <li>
                                    <a href="#">Manufacturer</a>
                                </li>
                                <li>
                                    <a href="#">Distributor</a>
                                </li>
                                <li></li>                              
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-file fa-fw"></i>
								Billing Administration <span class="caret"></span>
							</a>
                            <ul class="dropdown-menu" role="menu">
                            	<li></li>                            	                               
								<li>
                                    <a href="#">Create Bill</a>
                                </li>
                                <li>
                                    <a href="#">View Bill</a>
                                </li>                                                              
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-reorder fa-fw"></i>
								Order Administration<span class="caret"></span>
							</a>
                            <ul class="dropdown-menu" role="menu">
                            	<li></li>
                                <li>
                                    <a href="#">Account Registration</a>
                                </li>                                                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
												
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-credit-card  "></i> 
								Invoice Management <span class="caret"></span>
							</a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="#">Invoice Generation</a>
                                </li>
                                <li></li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bell fa-fw"></i>
								Alert Management <span class="caret"></span>
							</a>
                            <ul class="dropdown-menu" role="menu">
                            	
                            	<li>
                                    <a href="#">General Alert</a>
                                </li>
                                <li></li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
		                <li>
							<a href="logout.html">
							<i class="fa fa-sign-out fa-fw"></i> 
							Logout</a>
						</li>
		            </ul>
		        </nav>
		        <!-- /#sidebar-wrapper -->
  		<!-- Navigation Ends -->



<script>
    $(document).ready(function () {
    	  var trigger = $('.hamburger'),
    	      overlay = $('.overlay'),
    	     isClosed = false;

    	    trigger.click(function () {
    	      hamburger_cross();      
    	    });

    	    function hamburger_cross() {

    	      if (isClosed == true) {          
    	        overlay.hide();
    	        trigger.removeClass('is-open');
    	        trigger.addClass('is-closed');
    	        isClosed = false;
    	      } else {   
    	        overlay.show();
    	        trigger.removeClass('is-closed');
    	        trigger.addClass('is-open');
    	        isClosed = true;
    	      }
    	  }
    	  
    	  $('[data-toggle="offcanvas"]').click(function () {
    	        $('#wrapper').toggleClass('toggled');
    	  });  
    	});
    </script>




  
<!-- Large modal -->         
       <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="userDetails">
         <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
              <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">User Details</h4> 
                  </div>
                  <div class="modal-body">                                 
                           <!-- START Registration form -->                      
                           <div class="">
                                  <!-- Form header -->                    
                                 
                                  <div class="">
                                         <form role="form" id="formCheckPassword" data-toggle="validator">                                           
                                                <div class="row">
                                                       <div class="col-sm-6 form-group">
                                                              <label for="firstname" class="control-label">First Name <span class="required-field">*</span></label>
                                                              <div class="form-group">
                                                                     <input type="text" class="form-control" id="firstname" required="required" value="Admin" readonly="readonly">
                                                                     <span class="" aria-hidden="true"></span>
                                                              </div>                                                      
                                                       </div>
                                                      
                                                       <div class="col-sm-6 form-group">
                                                              <label for="lastname" class="control-label">Last Name<span class="required-field">*</span></label>
                                                              <div class="form-group">
                                                                     <input type="text" readonly="readonly" class="form-control" id="lastname" required="required">
                                                                     <span class="" aria-hidden="true"></span>
                                                              </div>
                                                       </div>
                                                </div>                                                                            
                                                                                                                                               
                                                <div class="row">                                               
                                                       <div class="col-sm-4 form-group">                                                             
                                                          <div class="form-group">
		                                                       <label for="email" class="control-label">Email <span class="required-field">*</span></label>
		                                                       <div class="form-group">
		                                                              <input type="email" class="form-control" id="email" required="required" readonly="readonly" value="test@domain.com">
		                                                              <span class="" aria-hidden="true"></span>
		                                                       </div>
	                                                		</div>                                                                                                      
                                                       </div>
                                                      
                                                       <div class="col-sm-4 form-group">
                                                              <label for="dob" class="control-label">Date Of Birth</label>
                                                              <div class="form-group">
		                                                         <div class='input-group date' class ='dateOfBirth' >
		                                                             <input type='text' class="form-control dateOfBirth" required="required" readonly="readonly"/>
		                                                             <span class="input-group-addon">
		                                                                 <span class="glyphicon glyphicon-calendar"></span>
		                                                             </span>
		                                                         </div>
		                                                     </div>                                                                                                               
                                                      </div>
                                                      
                                                       <div class="col-sm-4 form-group">
                                                           <label for="gender" class="control-label">Gender</label>
                                                                  <div class="form-group">
                                                                        <label class="radio-inline">
                                                                          <input type="radio" name="gender" value="Male" disabled="disabled"> Male
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                          <input type="radio" name="gender" disabled="disabled"  value="Female"> Female
                                                                        </label>
                                                                  </div>                                                                                                      
                                                       </div>                                                                                                       
                                                </div>
                                                
                                                <div class="row">                                               
                                                       <div class="col-sm-4 form-group">                                                
                                                			<div class="form-group">
                                                                <label for="address">Address<span class="required-field">*</span></label>
                                                                <textarea class="form-control" rows="5" id="address" required="required" disabled="disabled" ></textarea>
                                                             </div>
                                                		</div>
                                                		
                                                		<div class="col-sm-4 form-group">                                                
                                                			 <label for="newPassword" class="control-label">New Password<span class="required-field">*</span></label>
                                                              <div class="form-group">
                                                                     <input type="password" placeholder="Enter New Password" class="form-control" id="newPassword" name="newPassword" required="required">
                                                                     <span class="" aria-hidden="true"></span>
                                                              </div>
                                                		</div>
                                                		
                                                		<div class="col-sm-4 form-group">                                                
                                                			 <label for="confirmPassowrd" class="control-label">Confirm Password<span class="required-field">*</span></label>
                                                              <div class="form-group">
                                                                     <input type="password" placeholder="Confirm New Password"  class="form-control" id="confirmPassowrd" name="confirmPassowrd" required="required">
                                                                     <span class="" aria-hidden="true"></span>
                                                              </div>
                                                              <span class="alert alert-danger hidden" id="confirmPassowrdSpan">
											                       Password didn't matched.
											                  </span>
                                                		</div>
                                                </div>
                                                <input type="hidden" id="userName" value="${sessionScope.userID}" />
                                                <div class="form-group text-center">	                                                
                                                       <button type="button" class="btn btn-primary btn-block" id="updatePassword">Update Password</button>
                                                </div>
                                         </form>
                                  </div> 
                                  <div class="">
                                         <span class="required-field">*</span> - required field
                                  </div>
                           </div>                    
                           <!-- END Registration form -->                                         
                  </div>
                  <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
           </div>
         </div>
       </div>
       
    <script src="js/validatePassword.js"></script>   
        