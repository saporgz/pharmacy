$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2015 Q1',
            'Case': 2666,
            'Cat': 2548,
            'John Deere': 1984,
            'Komatsu': null,
            'Volvo': 2589
        }, {
            period: '2015 Q2',
            'Case': 2484,
            'Cat': 2650,
            'John Deere': 2858,
            'Komatsu': 1870,
            'Volvo': 2984
        }, {
            period: '2015 Q3',
            'Case': 3041,
            'Cat': 2845,
            'John Deere': 2540,
            'Komatsu': 3010,
            'Volvo': 2574
        }, {
            period: '2015 Q4',
            'Case': 3150,
            'Cat': 2989,
            'John Deere': 3150,
            'Komatsu': 3094,
            'Volvo': 3111
        }, {
            period: '2016 Q1',
            'Case': 3245,
            'Cat': 3341,
            'John Deere': 3098,
            'Komatsu': 3338,
            'Volvo': 3410
        }, {
            period: '2016 Q2',
            'Case': 2985,
            'Cat': 3220,
            'John Deere': 3540,
            'Komatsu': 3350,
            'Volvo': 3437
        }, {
            period: '2016 Q3',
            'Case': 3540,
            'Cat': 2350,
            'John Deere': 3512,
            'Komatsu': 3600,
            'Volvo': 3419
        }, {
            period: '2016 Q4',
            'Case': 3740,
            'Cat': 3636,
            'John Deere': 3650,
            'Komatsu': 3700,
            'Volvo': 3580
        }],
        xkey: 'period',
        ykeys: ['Case','Cat','John Deere', 'Komatsu', 'Volvo'],
        labels: ['Case','Cat','John Deere', 'Komatsu', 'Volvo'],
        pointSize: 2,
        colors: ['#a6d000', '#0070e7', '#F26C4F', '#ffab17', '#f4425c'],
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        colors: ['#a6d000', '#0070e7', '#F26C4F', '#ffab17', '#f4425c'],
        data: [{
            label: "John Deere",
            value: 42
        }, {
            label: "Cat",
            value: 15
        },
        {
            label: "Case",
            value: 24
        },
        {
            label: "Komatsu",
            value: 26
        },{
            label: "Volvo",
            value: 39
        }],
        resize: true
    });
    
});
