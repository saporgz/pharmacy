package com.saporgz.pharmacy.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.saporgz.pharmacy.am.model.Login;

public class LoginRowMapper implements RowMapper
{
    @Override
    public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Login loggedIn = new Login();       
    	loggedIn.setUserName(rs.getString("username"));
        return loggedIn;
    }

	
}