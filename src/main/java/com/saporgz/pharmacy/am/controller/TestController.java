package com.saporgz.pharmacy.am.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest")
@RestController
public class TestController {

	@RequestMapping("/ping")
	public String pingMe(){
		return "PInged";
	}
}
