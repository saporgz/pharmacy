package com.saporgz.pharmacy.am.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.saporgz.pharmacy.am.model.Login;
import com.saporgz.pharmacy.am.service.LoginService;

@Controller
public class LoginController {
	
	//private static final Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService;

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";

	@RequestMapping("/am")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "login";
	}
	
	@RequestMapping(value = "/loginAuthentication", method = RequestMethod.POST)
	public String home(HttpServletRequest request,
								 HttpServletResponse response, ModelMap model,
								 Login login
								 ) {
		String viewName="login";
		System.out.println(""+login.getUserName()+"-"+login.getPassword());
		try{
			boolean authenticateStatus=loginService.authenticate(login);
			if(authenticateStatus){
				viewName="home";
				HttpSession session = request.getSession();
				session.setAttribute("userID", login.getUserName());
			}else{
				login.setMessage("Wrong username and password");
				viewName=serveLoginPage(request,response,model,login);
			}
			model.addAttribute("login", login);
		}catch(Exception e){
			//logger.error("Exception in home() in LoginController class ",e);
		}
		return viewName;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String serveLoginPage(HttpServletRequest request,
								 HttpServletResponse response, ModelMap model,
								 Login login) {
		model.addAttribute("login", login);
		return "login";
	}
	
	@RequestMapping(value="/logout", method= RequestMethod.GET)
	 public String logout(HttpServletRequest request, HttpServletResponse response,  ModelMap model,
			 @RequestParam(required = false,value = "status") String status){				 
			 String viewName="login";
			 HttpSession session=request.getSession();  
	        session.invalidate(); 
	        model.addAttribute("logoutMessage","You have successfully logged out!");
	        if(null!=status && !status.isEmpty()){
	        	model.addAttribute("logoutMessage","Password changed successfully.");
	        }
	        return viewName;
	 }
	
	@RequestMapping(value = "/passwordModal", method = RequestMethod.GET)
	public String navigation(HttpServletRequest request,
								 HttpServletResponse response, ModelMap model) {
		return "passwordModal";
	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request,
								 HttpServletResponse response, ModelMap model) {	
		return "home";
	}
}
