package com.saporgz.pharmacy.am.dao.impl;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.saporgz.pharmacy.am.dao.LoginDao;
import com.saporgz.pharmacy.am.model.Login;
import com.saporgz.pharmacy.mapper.LoginRowMapper;

@Repository
public class LoginDaoImpl extends JdbcDaoSupport implements LoginDao {
	
	private static final Logger logger = Logger.getLogger(LoginDaoImpl.class);
	
	@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	@Override
	public boolean authenticate(Login login) {
		System.out.println("At DAOIMPL");
		
		boolean  authenticateStatus=false;
		String sql = "SELECT username FROM user WHERE username = ? AND password=?";

		@SuppressWarnings("unchecked")
		Login loginReturned = (Login)getJdbcTemplate().queryForObject(
				sql, new Object[]{login.getUserName(), login.getPassword()}, new LoginRowMapper());
		
		if(loginReturned!=null && loginReturned.getUserName()!=null){
			authenticateStatus=true;
		}
			
		
		
		
		return authenticateStatus;
	}

	

}
