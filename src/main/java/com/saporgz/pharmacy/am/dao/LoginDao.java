package com.saporgz.pharmacy.am.dao;

import com.saporgz.pharmacy.am.model.Login;

public interface LoginDao {

	boolean authenticate(Login login);

}
