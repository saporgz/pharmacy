package com.saporgz.pharmacy.am.service;

import com.saporgz.pharmacy.am.model.Login;

public interface LoginService {

	boolean authenticate(Login login);
	

}
