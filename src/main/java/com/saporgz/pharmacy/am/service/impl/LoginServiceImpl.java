package com.saporgz.pharmacy.am.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saporgz.pharmacy.am.dao.LoginDao;
import com.saporgz.pharmacy.am.model.Login;
import com.saporgz.pharmacy.am.service.LoginService;



@Service
public class LoginServiceImpl implements LoginService {
	
	private static final Logger logger = Logger.getLogger(LoginServiceImpl.class);
	
	@Autowired
	LoginDao loginDao;

	public boolean authenticate(Login login) {
		return loginDao.authenticate(login);
		
	}

}
