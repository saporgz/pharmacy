<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title>Dashboard Page</title>
 
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
     
    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
 
    <!-- Custom Fonts -->
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    
        <!-- Morris Charts CSS -->
    <link href="css/morris.css" rel="stylesheet">
 	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  	
  	<script src="js/jquery.min.js"></script> 
  	<script> 
		$.get("passwordModal.html", function(data){
				$("#passModal").replaceWith(data);
			});
	</script>
</head>
 
<body>
 
    <div id="wrapper">
    
        <div id="passModal"></div>
        
  		<div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
    			<span class="hamb-middle"></span>
				<span class="hamb-bottom"></span>
            </button>
            <div class="container">
              <div class="row">
              	<div class="col-lg-12">
              	<h4 class="page-header">DASHBOARD</h4> 
                    <div class="col-lg-6">                      
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Quarterly Revenue Graph
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="morris-area-chart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    
                    <div class="col-lg-6">                    
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Current Fleet Strength
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="morris-donut-chart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                  </div> 
                </div>
              </div>
                <!-- /.row -->
        </div>    
           
    </div>
    <!-- /#wrapper -->
    
    
    <!-- Bootstrap Core JavaScript -->
    
 	 	
 	<script src="js/bootstrap.min.js"></script> 
    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>    
    
    <!-- Morris Charts JavaScript -->
    <script src="js/raphael.min.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/myData.js"></script>
    
</body> 
</html>